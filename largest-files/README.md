# largest-files

largest-files is a utility for Linux that finds the largest files in a
directory and its subdirectories.

# Usage

If you only have a few dozen or a few hundred files you can just run:

    largest-files

If you have a large number of files, you might want to limit the
output. To print the 20 largest files in this directory and its
subdirectories:

    largest-files | tail -n20
