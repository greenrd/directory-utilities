module Main (main) where

import Control.Monad.Catch (MonadThrow)
import qualified Data.ByteString as BS
import Data.List (sort)
import Data.Map.Strict (Map, insert, lookup)
import Data.Maybe (fromMaybe)
import qualified Data.Text.IO as T
import Data.Text (commonPrefixes, pack, Text)
import System.IO (isEOF)
import System.OsString.Posix (decodeUtf, fromBytes)
import System.OsPath.Posix (PosixPath, splitDirectories)
import Prelude hiding (lookup)
import Control.Monad (unless)

toText :: MonadThrow m => PosixPath -> m Text
toText pp = do
  s <- decodeUtf pp
  pure $ pack s

printMatch :: PosixPath -> PosixPath -> IO ()
printMatch nextPath prevPath =
  do
    nextPathT <- toText nextPath
    prevPathT <- toText prevPath
    let (common, next, prev) = fromMaybe ("", nextPathT, prevPathT) $ commonPrefixes nextPathT prevPathT
    T.putStr "Within "
    T.putStr common
    T.putStr " : "
    T.putStr next
    putStr " is a duplicate of "
    T.putStrLn prev        

process :: Map [PosixPath] PosixPath -> IO ()
process prevSeen = do
  noMore <- isEOF
  unless noMore $ do
    nextPath <- BS.getLine
    nextString <- fromBytes nextPath
    let dirs = splitDirectories nextString
        sortedDirs = sort dirs
    ret <- case lookup sortedDirs prevSeen of
      Nothing -> pure $ insert sortedDirs nextString prevSeen
      Just prevActualPath -> prevSeen <$ printMatch nextString prevActualPath
    process ret

main :: IO ()
main = do
  process mempty