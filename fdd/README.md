# Find Duplicate Directories

`fdd` is a utility for Linux that finds pairs of directory paths that
contain all the same names, such as `a/b/c` and `b/a/c`.  You could
then merge these directories together.

## Usage

    find -type d | fdd
