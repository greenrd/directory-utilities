# file-away

File-away is a utility for Linux that assists in creating symlinks in
subdirectories. It open the specified file using the associated
application, and then pops up a dialog box that allows you to select a
subdirectory to create the symlink in.

Before it does that, however, it first checks whether any subdirectory
already contains a symlink to the specified file - unless the `-n`
option is passed in.

# Usage

## Basic usage

    f [-n] $FILENAME
    f -m $DIRECTORY
    f -u $DIRECTORY
	
### Options

| Option | Description |
| --- | --- |
| -n | Skip check for whether an existing symlink exists |
| -m | Print all files in $DIRECTORY that already have a symlink pointing to them from the current directory or a (recursive) subdirectory of it |
| -u | Print all files in $DIRECTORY that do not already have a symlink pointing to them from the current directory or a (recursive) subdirectory of it |
	
## Starting navigation in a previous directory

Each time you run `f` in filename mode, it outputs the directory that
it created the symlink in, as an absolute path. You can copy and paste
this directory as a second argument to start the navigation in that
directory for the next file:

    f [-n] $FILENAME $PREVIOUS_DIRECTORY
