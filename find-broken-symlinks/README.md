# find-broken-symlinks

find-broken-symlinks is a utility for Linux that lists all the broken
symlinks (symbolic links that point to a destination that does not
exist) in the current directory or any of its subdirectories.

It's so simple that it could be just an alias, but I decided to
implement it as a script to give it the maximum freedom for potential
future improvements.
